package com.aem.training.winkle.site.core.servlets;

        import java.io.IOException;
        import javax.servlet.Servlet;
        import javax.servlet.ServletException;

        import com.aem.training.winkle.site.core.models.Demo;
        import org.apache.sling.api.SlingHttpServletRequest;
        import org.apache.sling.api.SlingHttpServletResponse;
        import org.apache.sling.api.servlets.HttpConstants;
        import org.apache.sling.api.servlets.SlingAllMethodsServlet;
        import org.json.JSONException;
        import org.json.JSONObject;
        import org.osgi.framework.Constants;
        import org.osgi.service.component.annotations.Component;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;

@Component(service=Servlet.class,
        property={
        Constants.SERVICE_DESCRIPTION + "=Test Post Servlet",
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths="+ "/bin/Myform",
                "sling.servlet.extensions=" + "json"

        })
public class Myform extends SlingAllMethodsServlet  {
    private static final long serialVersionUID = 1L;
    private  final Logger logger= LoggerFactory.getLogger(Myform.class);

    @Override
    protected void doPost(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", name);
            logger.info("hello "+name);
            obj.put("desc", description);
            logger.info("this is the "+description);
        }
        catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        resp.setContentType("application/json");
        resp.setCharacterEncoding("utf-8");
        resp.getWriter().write(obj.toString());
    }

}

