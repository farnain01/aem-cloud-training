package com.aem.training.winkle.site.core.models;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.List;


@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public interface Banner {
    @Inject
    List<Demo.Mydetails> getbannerdetails();

    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

    class bannerdetails {

        @ValueMapValue
        String heading;

        public String getHeading() {
            return heading;
        }
        @ValueMapValue
        String link;

        public String getLink() {
            return link;
        }
    }
}
