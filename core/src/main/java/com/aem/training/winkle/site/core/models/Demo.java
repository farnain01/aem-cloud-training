package com.aem.training.winkle.site.core.models;

import org.apache.sling.api.resource.Resource;
        import org.apache.sling.models.annotations.DefaultInjectionStrategy;
        import org.apache.sling.models.annotations.Model;
        import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
        import java.util.List;


@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public interface Demo {

    @Inject
    List<Mydetails> getMydetails();

    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

    class Mydetails {
        private  final Logger logger= LoggerFactory.getLogger(Mydetails.class);
        @ValueMapValue
        String name;

        public String getName() {
            logger.info("hello "+name);
            return name;
        }
    }
}